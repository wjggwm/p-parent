# [Spring Cloud敏捷开发框架](https://gitee.com/likun_557/p-parent "查看代码")

1、概述

> 用来快速开发Spring Cloud项目  
> 相关技术：maven3.3.9、jdk1.8、springboot2.0.4、springcloud Finchley.SR1、mybatis、freemarker  
> 框架实现了读写分离、数据库基本的各种操作、自动生成代码工具等

2、项目介绍
- p-base:
    - p-base-comm:框架基础的一些类
    - y-base-jdbc:数据库操作相关类
- p-code:
    - p-code-generate:代码生成器
- y-cloud:  
    - p-cloud-discovery:服务注册中心
    - p-cloud-gateway:服务网关
- y-core: 表示一个业务模块
    - p-core-comm:业务模块公共类
    - p-core-service:业务模块restful服务接口
    - p-core-api:业务模块对外的feign接口类，其他模块如果需要调用当前模块的服务，只需引用该jar即可
    - p-core-test:测试模块，通过feign来调用p-core-service
    
3、端口
- p-cloud-discovery:7001
- p-cloud-gateway:8001
- p-core-service:9001
- p-core-test:9002

4、启动顺序
- p-cloud-discovery
- p-cloud-gateway
- p-core-service
- p-core-test