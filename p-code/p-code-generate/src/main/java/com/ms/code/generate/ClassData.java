package com.ms.code.generate;

import lombok.*;

/**
 * <b>description</b>： <br>
 * <b>time</b>：2018-08-07 10:03 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class ClassData {
    //类简称，如DemoModel
    protected String simpleName;
    //包名称，如com.ms.core.comm.admin.model
    protected String pkgName;
    //完整类名，如com.ms.core.comm.admin.model.DemoModel
    protected String name;
    //对象名称,如 demoModel
    protected String objName;
}
