package com.ms.code.generate;

import lombok.*;

/**
 * <b>description</b>：表配置 <br>
 * <b>time</b>：2018-08-06 18:04 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Data
@NoArgsConstructor
@Builder
@ToString
@AllArgsConstructor
public class TableConfig {
    //表名
    private String tableName = "demo";
    //jdbcurl
    private String jdbcUrl = "jdbc:mysql://192.168.10.140:3306/www_yijiedai_com?characterEncoding=UTF-8";
    //驱动
    private String jdbcDriver = "com.mysql.jdbc.Driver";
    //数据库用户名
    private String jdbcUserName = "www_yijiedai_com";
    //数据库密码
    private String jdbcPassword = "yijiedaitest";
    //描述
    private String remark;
}
