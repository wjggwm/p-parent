package com.ms.code.generate;

import lombok.*;

import java.util.List;

/**
 * <b>description</b>：表信息 <br>
 * <b>time</b>：2018-08-06 17:32 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Data
@ToString
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class TableModel {
    /**
     * 表名
     */
    private String name;
    /**
     * 备注
     */
    private String remark;
    /**
     * 字段列表
     */
    private List<FieldModel> fieldModelList;
    /**
     * 主键字段
     */
    private FieldModel primaryFieldModel;
}
