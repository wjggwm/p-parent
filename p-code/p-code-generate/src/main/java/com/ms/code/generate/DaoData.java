package com.ms.code.generate;

import lombok.*;

/**
 * <b>description</b>：dao接口数据 <br>
 * <b>time</b>：2018-08-07 09:57 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class DaoData {
    private ClassData classData;
    private CodeFile codeFile;
}
