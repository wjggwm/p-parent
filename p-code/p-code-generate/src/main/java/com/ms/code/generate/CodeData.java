package com.ms.code.generate;

import lombok.*;

/**
 * <b>description</b>：代码其他信息 <br>
 * <b>time</b>：2018-08-07 09:43 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class CodeData {
    //表配置
    private TableConfig tableConfig;
    //代码存放路径
    private String codeDirPath;
    //是否覆盖文件
    private boolean overwriteFile;
    //项目名称
    private String prjName;
    //模块名称
    private String moduleName;
    //作者
    private String author;
    //描述信息
    private String remark;
    //系统当前时间
    private String dateTime;
    //表信息
    private TableModel tableModel;

    private ModelData modelData;
    private ServiceData serviceData;
    private ServiceImplData serviceImplData;
    private DaoData daoData;
    private DaoImplData daoImplData;
    private MapperData mapperData;
    private SqlMapData sqlMapData;
}
