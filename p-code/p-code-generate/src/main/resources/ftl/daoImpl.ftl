package ${daoImplData.classData.pkgName};

import com.ms.base.jdbc.dao.BaseDaoImpl;
import com.ms.base.jdbc.dao.IBaseMapper;
import ${modelData.classData.name};
import ${daoData.classData.name};
import ${mapperData.classData.name};
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * <b>description</b>：${remark}数据访问实现类 <br>
 * <b>time</b>：${dateTime} <br>
 * <b>author</b>：${author}
 */
@Component
public class ${daoImplData.classData.simpleName} extends BaseDaoImpl<${modelData.classData.simpleName}> implements IDemoDao {
    @Autowired
    private ${mapperData.classData.simpleName} ${mapperData.classData.objName};

    @Override
    protected String getPrimaryKeyName() {
        return "${tableModel.primaryFieldModel.name}";
    }

    @Override
    public IBaseMapper<${modelData.classData.simpleName}> getBaseMapper() {
        return this.demoMapper;
    }
}