package ${serviceData.classData.pkgName};

import com.ms.base.jdbc.service.IBaseService;
import ${modelData.classData.name};

/**
* <b>description</b>：${remark}业务接口 <br>
* <b>time</b>：${dateTime} <br>
* <b>author</b>：${author}
*/
public interface ${serviceData.classData.simpleName} extends IBaseService<${modelData.classData.simpleName}> {
}