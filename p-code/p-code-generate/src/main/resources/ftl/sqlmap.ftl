<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="${mapperData.classData.name}">

    <!-- 插入 -->
    <#if tableModel.primaryFieldModel!=null>
    <insert id="insert" parameterType="${modelData.classData.name}" keyProperty="${tableModel.primaryFieldModel.name}" useGeneratedKeys="true">
    <#else>
    <insert id="insert" parameterType="${modelData.classData.name}">
    </#if>
        <![CDATA[ INSERT INTO `${tableModel.name}` ]]>
        <trim prefix="(" suffix=")" suffixOverrides=",">
            <#list tableModel.fieldModelList as fieldModel>
            <if test="${fieldModel.name}!=null and ${fieldModel.name}!=''">
                <![CDATA[ `${fieldModel.name}`, ]]>
            </if>
            </#list>
        </trim>
        <![CDATA[ VALUES ]]>
        <trim prefix="(" suffix=")" suffixOverrides=",">
            <#list tableModel.fieldModelList as fieldModel>
            <if test="${fieldModel.name}!=null and ${fieldModel.name}!=''">
                <![CDATA[ ${'#'}{${fieldModel.name}}, ]]>
            </if>
            </#list>
        </trim>
    </insert>

    <!-- 批量插入 -->
    <insert id="insertBatch" parameterType="map">
        <![CDATA[ INSERT INTO `${tableModel.name}` (<#list tableModel.fieldModelList as fieldModel>`${fieldModel.name}`<#if (tableModel.fieldModelList?size>fieldModel_index+1)>, </#if></#list>) VALUES ]]>
        <foreach collection="list" separator="," item="item">
            (<#list tableModel.fieldModelList as fieldModel>${'#'}{item.${fieldModel.name}}<#if (tableModel.fieldModelList?size>fieldModel_index+1)>, </#if></#list>)
        </foreach>
    </insert>

    <!-- 更新 -->
    <update id="update" parameterType="${modelData.classData.name}">
        <![CDATA[ UPDATE `${tableModel.name}` ]]>
        <set>
            <#list tableModel.fieldModelList as fieldModel>
            <if test="${fieldModel.name}!=null and ${fieldModel.name}!=''">
                <![CDATA[ `${fieldModel.name}` = ${'#'}{${fieldModel.name}}, ]]>
            </if>
            </#list>
        </set>
        <where>
            <#list tableModel.fieldModelList as fieldModel>
            <if test="${fieldModel.name}!=null and ${fieldModel.name}!=''">
                <![CDATA[ AND `${fieldModel.name}` = ${'#'}{${fieldModel.name}} ]]>
            </if>
            </#list>
        </where>
    </update>

    <!-- 更新 -->
    <update id="updateByMap" parameterType="map">
        <![CDATA[ UPDATE `${tableModel.name}` ]]>
        <set>
            <#list tableModel.fieldModelList as fieldModel>
            <if test="${fieldModel.name}!=null and ${fieldModel.name}!=''">
                <![CDATA[ `${fieldModel.name}` = ${'#'}{${fieldModel.name}}, ]]>
            </if>
            </#list>
        </set>
        <where>
            <#list tableModel.fieldModelList as fieldModel>
            <if test="${fieldModel.name}!=null and ${fieldModel.name}!=''">
                <![CDATA[ AND `${fieldModel.name}` = ${'#'}{${fieldModel.name}} ]]>
            </if>
            </#list>
        </where>
    </update>

    <!-- 删除 -->
    <delete id="delete" parameterType="map">
        <![CDATA[
			DELETE FROM `${tableModel.name}`
		]]>
        <where>
        <#list tableModel.fieldModelList as fieldModel>
            <if test="${fieldModel.name}!=null and ${fieldModel.name}!=''">
                <![CDATA[ AND `${fieldModel.name}` = ${'#'}{${fieldModel.name}} ]]>
            </if>
        </#list>
        </where>
    </delete>

    <sql id="findSql">
        <where>
        <#list tableModel.fieldModelList as fieldModel>
            <if test="${fieldModel.name}!=null and ${fieldModel.name}!=''">
                <![CDATA[ AND `${fieldModel.name}` = ${'#'}{${fieldModel.name}} ]]>
            </if>
        </#list>
        </where>
    </sql>

    <!-- 查询行数 -->
    <select id="getModelListCount" parameterType="map" resultType="long">
        <![CDATA[
			SELECT COUNT(*) FROM `${tableModel.name}` a
		]]>
        <include refid="findSql"/>
    </select>

    <!-- 查询记录 -->
    <select id="getModelList" parameterType="map" resultType="${modelData.classData.name}">
        <include refid="global.pageStart"/>
        <![CDATA[
            SELECT
            <#list tableModel.fieldModelList as fieldModel>
            `${fieldModel.name}`<#if (tableModel.fieldModelList?size>fieldModel_index+1)>,</#if>
            </#list>
            FROM
            `${tableModel.name}` a
		]]>
        <include refid="findSql"/>
        <include refid="global.globalSortSql"/>
        <include refid="global.pageEnd"/>
    </select>

</mapper>