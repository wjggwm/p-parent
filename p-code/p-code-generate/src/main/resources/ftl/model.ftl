package ${modelData.classData.pkgName};

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * <b>description</b>：${remark} <br>
 * <b>time</b>：${dateTime} <br>
 * <b>author</b>：${author}
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ${modelData.classData.simpleName} implements Serializable {
    private static final long serialVersionUID = 1L;
    <#list tableModel.fieldModelList as fieldModel>
    /**
     * ${fieldModel.remark}
     */
    private ${fieldModel.javaTypeName} ${fieldModel.name};
    </#list>
}