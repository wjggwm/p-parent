package com.ms.base.jdbc.dao;

import com.ms.base.comm.page.PageModel;

import java.util.List;
import java.util.Map;

/**
 * <b>description</b>： <br>
 * <b>time</b>：2018-07-26 16:56 <br>
 * <b>author</b>： ready likun_557@163.com
 */
public interface IBaseDao<T> extends IDbType {

    String SQL_IDLIST_KEY = "idList";
    String SQLMAPID_GETMODELLISTCOUNT = "getModelListCount";
    String SQLMAPID_GETMODELLIST = "getModelList";

    /**
     * 插入
     *
     * @param model
     */
    T insert(T model) throws Exception;

    /**
     * 更新
     *
     * @param model
     * @return
     */
    int update(T model) throws Exception;

    /**
     * 根据传入的map进行更新
     *
     * @param map 参数
     * @return
     */
    int updateByMap(Map<String, Object> map) throws Exception;

    /**
     * 删除数据
     *
     * @param map 参数
     * @return
     */
    int delete(Map<String, Object> map) throws Exception;

    /**
     * 根据对象id删除数据
     *
     * @param id 对象id
     * @return 返回影响行数
     */
    int deleteById(Object id) throws Exception;

    /**
     * 获取记录行数
     *
     * @param map 查询条件
     * @return
     */
    long getModelListCount(Map<String, Object> map) throws Exception;

    /**
     * 获取记录列表
     *
     * @param map 查询条件
     * @return
     */
    List<T> getModelList(Map<String, Object> map) throws Exception;

    /**
     * 获取记录行数
     *
     * @param map      查询条件
     * @param sqlMapId
     * @return
     * @throws Exception
     */
    long getModelListCount(String sqlMapId, Map<String, Object> map) throws Exception;

    /**
     * 获取model列表
     *
     * @param sqlMapId
     * @param map      查询条件
     * @return
     * @throws Exception
     */
    List getModelList(String sqlMapId, Map<String, Object> map) throws Exception;


    /**
     * 根据对象id查询数据
     *
     * @param id 对象id
     * @return 返回id对应的对象
     */
    <K> T getModelById(K id) throws Exception;

    /**
     * 查询id列表对应的对象列表
     *
     * @param idList id列表
     * @return
     */
    List<T> getModelsByIds(List idList) throws Exception;

    /**
     * 根据idList获取对象列表的map，key为对象的id，value为对象
     *
     * @param idList id列表
     * @param <K>    id的类型
     * @return id->item 列表
     */
    <K> Map<K, T> getModelMapByIds(List<K> idList) throws Exception;

    /**
     * 获取一个对象
     *
     * @param paramMap
     * @return
     */
    T getModelOne(Map<String,Object> paramMap) throws Exception;

    /**
     * 分页查询
     *
     * @param map  查询条件
     * @param page 当前页数
     * @param rows 每页行数
     * @return 查询结果
     * @throws Exception
     */
    PageModel<T> getPageModel(Map<String,Object> map, int page, int rows) throws Exception;

    /**
     * 查询列表
     * @param map 查询条件
     * @param skip 跳过的行数
     * @param rows 行数
     * @return
     * @throws Exception
     */
    List<T> getModelList(Map<String,Object> map, int skip, int rows) throws Exception;
}
