package com.ms.base.jdbc.page;

import com.ms.base.comm.page.PageConst;
import com.ms.base.comm.page.PageModel;
import com.ms.base.comm.util.FrameUtil;
import com.ms.base.jdbc.dao.DbType;

import java.util.HashMap;
import java.util.Map;

/**
 * <b>description</b>：分页工具类 <br>
 * <b>time</b>：2018-08-03 13:54 <br>
 * <b>author</b>： ready likun_557@163.com
 */
public class PageUtil {

    public static final String DBTYPE_KEY = "dbType";
    public static final String PAGE_KEY = PageConst.PAGE_KEY;
    public static final String ROWS_KEY = PageConst.ROWS_KEY;
    public static final String ROWSTARTINDEX_KEY = "rowstartindex";
    public static final String ROWENDINDEX_KEY = "rowendindex";
    public static final int PAGE_SIZE = 20;

    /**
     * 分页
     *
     * @param page   获取分页分页数据的对象
     * @param map    查询条件
     * @param dbType 数据源类型
     * @return
     * @throws Exception
     */
    public static PageModel getPageModel(IPage page, Map map, DbType dbType) throws Exception {
        return getPageModel(page, map, null, dbType);
    }


    /**
     * 分页
     *
     * @param page     获取分页分页数据的对象
     * @param map      查询条件
     * @param sqlMapId sqlmapid
     * @param dbType   数据源类型
     * @return
     * @throws Exception
     */
    public static PageModel getPageModel(IPage page, Map map, String sqlMapId, DbType dbType) throws Exception {
        PageModel pageModel = new PageModel();
        if (map == null) {
            map = new HashMap();
        }
        Integer currentPage = map.get(PAGE_KEY) == null ? null : Integer
                .parseInt(map.get(PAGE_KEY).toString());
        Integer pageSize = map.get(ROWS_KEY) == null ? null : Integer
                .parseInt(map.get(ROWS_KEY).toString());
        if (currentPage == null) {
            currentPage = 1;
        }
        if (pageSize == null) {
            pageSize = PAGE_SIZE;
        }
        map.put(DBTYPE_KEY, dbType.name());
        pageModel.setPageSize(pageSize);
        pageModel.setCurrentPage(currentPage);
        pageModel.setCount(page.getPageCount(map, sqlMapId));

        if (DbType.isMysql(dbType)) {
            if (pageModel.getStartIndex() <= 0) {
                map.put(ROWSTARTINDEX_KEY, 0);
                map.put(ROWENDINDEX_KEY, 0);
            } else {
                map.put(ROWSTARTINDEX_KEY, pageModel.getStartIndex() - 1);
                map.put(ROWENDINDEX_KEY, pageModel.getPageSize());
            }
        } else {
            map.put(ROWSTARTINDEX_KEY, pageModel.getStartIndex());
            map.put(ROWENDINDEX_KEY, pageModel.getEndIndex());
        }
        if (pageModel.getCount() >= 1) {
            pageModel.setDataList(page.getPageList(map, sqlMapId));
        } else {
            pageModel.setDataList(FrameUtil.newArrayList());
        }
        return pageModel;
    }


    /**
     * 获取一个pageModel
     *
     * @param page  第几页
     * @param rows  每页行数
     * @param count 总行数
     * @return
     */
    public static PageModel getPageModel(int page, int rows, int count) {
        PageModel PageModel = new PageModel();
        PageModel.setPageSize(rows);
        PageModel.setCurrentPage(page);
        PageModel.setCount(count);
        return PageModel;
    }

    /**
     * 处理map参数
     *
     * @param map    参数
     * @param dbType 数据库类型
     * @return
     */
    public static Map<String, Object> disposeParamMap(Map<String, Object> map, DbType dbType) {
        if (map != null) {
            map.put(DBTYPE_KEY, dbType.name());
            Integer currentPage = map.get(PAGE_KEY) == null ? null : Integer.parseInt(map.get(PAGE_KEY).toString());
            Integer pageSize = map.get(ROWS_KEY) == null ? null : Integer.parseInt(map.get(ROWS_KEY).toString());
            if (currentPage != null && pageSize != null) {
                if (DbType.isMysql(dbType)) {
                    map.put(ROWSTARTINDEX_KEY, (currentPage - 1) * pageSize);
                    map.put(ROWENDINDEX_KEY, pageSize);
                } else {
                    map.put(ROWSTARTINDEX_KEY, (currentPage - 1) * pageSize);
                    map.put(ROWENDINDEX_KEY, currentPage * pageSize);
                }
            }
        }
        return null;
    }

    /**
     * 处理查询参数
     *
     * @param map    查询产生
     * @param dbType 数据库类型
     * @param skip   跳过多少条
     * @param rows   取多少条
     * @return
     */
    public static Map<String, Object> disposeParamMap(Map<String, Object> map, DbType dbType, int skip, int rows) {
        if (map == null) {
            map = FrameUtil.newHashMap();
        }
        map.put(DBTYPE_KEY, dbType.name());
        if (DbType.isMysql(dbType)) {
            map.put(ROWSTARTINDEX_KEY, skip);
            map.put(ROWENDINDEX_KEY, rows);
        } else {
            map.put(ROWSTARTINDEX_KEY, skip);
            map.put(ROWENDINDEX_KEY, skip + rows);
        }
        return map;
    }
}
