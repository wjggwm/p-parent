package com.ms.base.jdbc.service;

import com.ms.base.jdbc.datasource.DsType;
import com.ms.base.jdbc.dao.IBaseDao;
import com.ms.base.comm.page.PageModel;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * <b>description</b>： <br>
 * <b>time</b>：2018-07-26 16:56 <br>
 * <b>author</b>： ready likun_557@163.com
 */
public abstract class BaseServiceImpl<T> implements IBaseService<T> {

    @Transactional
    @Override
    public T insert(T model) throws Exception {
        return this.getBaseDao().insert(model);
    }

    @Transactional
    @Override
    public int update(T model) throws Exception {
        return this.getBaseDao().update(model);
    }

    @Transactional
    @Override
    public int deleteById(Object id) throws Exception {
        return this.getBaseDao().deleteById(id);
    }

    @Override
    public long getModelListCount(Map<String, Object> map, DsType dsType) throws Exception {
        return this.getBaseDao().getModelListCount(map);
    }

    @Override
    public List<T> getModelList(Map<String, Object> map, DsType dsType) throws Exception {
        return this.getBaseDao().getModelList(map);
    }

    @Override
    public <K> T getModelById(K id, DsType dsType) throws Exception {
        return this.getBaseDao().getModelById(id);
    }

    @Override
    public List<T> getModelsByIds(List idList, DsType dsType) throws Exception {
        return this.getBaseDao().getModelsByIds(idList);
    }

    @Override
    public <K> Map<K, T> getModelMapByIds(List<K> idList, DsType dsType) throws Exception {
        return this.getBaseDao().getModelMapByIds(idList);
    }

    @Override
    public T getModelOne(Map<String, Object> map, DsType dsType) throws Exception {
        return this.getBaseDao().getModelOne(map);
    }

    @Override
    public PageModel<T> getPageModel(Map map, int page, int rows, DsType dsType) throws Exception {
        return this.getBaseDao().getPageModel(map, page, rows);
    }

    @Override
    public List<T> getModelList(Map<String, Object> map, int skip, int rows, DsType dsType) throws Exception {
        return this.getBaseDao().getModelList(map, skip, rows);
    }

    public abstract IBaseDao<T> getBaseDao();
}
