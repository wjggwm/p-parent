package com.ms.base.jdbc.dao;

/**
 * <b>description</b>：数据库类型接口 <br>
 * <b>time</b>：2018-08-03 15:51 <br>
 * <b>author</b>： ready likun_557@163.com
 */
public interface IDbType {
    DbType dbType();
}
