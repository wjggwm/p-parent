package com.ms.base.jdbc.service;

import com.ms.base.jdbc.datasource.DsType;
import com.ms.base.comm.page.PageModel;

import java.util.List;
import java.util.Map;

/**
 * <b>description</b>：业务操作基类 <br>
 * <b>time</b>：2018-07-26 16:56 <br>
 * <b>author</b>： ready likun_557@163.com
 */
public interface IBaseService<T> extends IService {
    /**
     * 插入
     *
     * @param model
     */
    T insert(T model) throws Exception;

    /**
     * 更新
     *
     * @param model
     * @return
     */
    int update(T model) throws Exception;

    /**
     * 根据对象id删除数据
     *
     * @param id 对象id
     * @return 返回影响行数
     */
    int deleteById(Object id) throws Exception;

    /**
     * 获取记录行数
     *
     * @param map    查询条件
     * @param dsType 数据源类型
     * @return
     */
    long getModelListCount(Map<String, Object> map, DsType dsType) throws Exception;

    /**
     * 获取记录列表
     *
     * @param map 查询条件
     * @return
     */
    List<T> getModelList(Map<String, Object> map, DsType dsType) throws Exception;


    /**
     * 根据对象id查询数据
     *
     * @param id 对象id
     * @return 返回id对应的对象
     */
    <K> T getModelById(K id, DsType dsType) throws Exception;

    /**
     * 查询id列表对应的对象列表
     *
     * @param idList id列表
     * @return
     */
    List<T> getModelsByIds(List idList, DsType dsType) throws Exception;

    /**
     * 根据idList获取对象列表的map，key为对象的id，value为对象
     *
     * @param idList id列表
     * @param <K>    id的类型
     * @return id->item 列表
     */
    <K> Map<K, T> getModelMapByIds(List<K> idList, DsType dsType) throws Exception;

    /**
     * 获取一个对象
     *
     * @param paramMap
     * @return
     */
    T getModelOne(Map<String, Object> paramMap, DsType dsType) throws Exception;

    /**
     * 分页查询
     *
     * @param map    查询条件
     * @param page   当前页数
     * @param rows   每页行数
     * @param dsType 主从查询标志
     * @return
     * @throws Exception
     */
    PageModel<T> getPageModel(Map<String, Object> map, int page, int rows, DsType dsType) throws Exception;

    /**
     * 查询列表
     *
     * @param map    查询条件
     * @param skip   跳过的行数
     * @param rows   行数
     * @param dsType 主从查询标志
     * @return
     * @throws Exception
     */
    List<T> getModelList(Map<String, Object> map, int skip, int rows, DsType dsType) throws Exception;
}
