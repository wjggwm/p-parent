package com.ms.base.jdbc.datasource;

/**
 * <b>description</b>：datasource上线文捕获器 <br>
 * <b>time</b>：2018-07-26 13:10 <br>
 * <b>author</b>： ready likun_557@163.com
 */
public class DsContextHolder {

    private static ThreadLocal<DsType> contextHolder = new ThreadLocal<DsType>();

    public static void master(){
        contextHolder.set(DsType.MASTER);
    }
    public static void slave(){
        contextHolder.set(DsType.SLAVE);
    }

    public static DsType getDsType() {
        return contextHolder.get() == null ? DsType.MASTER : contextHolder.get();
    }

    public static void clearDsType() {
        contextHolder.remove();
    }

    /**
     * 是否是主库
     *
     * @return
     */
    public static boolean isMaster() {
        DsType dsType = getDsType();
        return dsType == DsType.MASTER;
    }
}
