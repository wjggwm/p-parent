package com.ms.base.jdbc.dao;

import java.util.List;
import java.util.Map;

/**
 * <b>description</b>： <br>
 * <b>time</b>：2018-07-26 16:48 <br>
 * <b>author</b>： ready likun_557@163.com
 */
public interface IBaseMapper<T> {
    /**
     * 插入
     *
     * @param model
     */
    void insert(T model);

    /**
     * 更新
     *
     * @param model
     * @return
     */
    int update(T model);

    /**
     * 根据传入的map进行更新
     *
     * @param map 参数
     * @return
     */
    int updateByMap(Map<String, Object> map);

    /**
     * 删除数据
     *
     * @param map 参数
     * @return
     */
    int delete(Map<String, Object> map);

    /**
     * 获取记录行数
     *
     * @param map 查询条件
     * @return
     */
    long getModelListCount(Map<String, Object> map);

    /**
     * 获取记录列表
     *
     * @param map 查询条件
     * @return
     */
    List<T> getModelList(Map<String, Object> map);
}
