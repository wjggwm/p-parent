package com.ms.base.comm.cloud;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * <b>description</b>： <br>
 * <b>time</b>：2018-08-02 10:35 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseException extends RuntimeException {
    /**
     * 错误代码
     */
    private String code;
    /**
     * 子代码
     */
    private String subCode;
    /**
     * 信息提示
     */
    private String msg;
    /**
     * 扩展数据
     */
    private Map extData;
}
