package com.ms.base.comm.page;

/**
 * <b>description</b>：分页常量 <br>
 * <b>time</b>：2018-08-06 15:27 <br>
 * <b>author</b>： ready likun_557@163.com
 */
public class PageConst {
    public static final String PAGE_KEY = "page";
    public static final String ROWS_KEY = "rows";
    public static final int PAGE_SIZE = 20;
}
