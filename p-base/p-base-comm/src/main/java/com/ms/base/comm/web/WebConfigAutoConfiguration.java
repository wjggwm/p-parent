package com.ms.base.comm.web;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;

/**
 * <b>description</b>：web基础配置，自动状态加载 <br>
 * <b>time</b>：2018-07-27 10:26 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Configuration
@EnableConfigurationProperties(WebConfigProperties.class)
public class WebConfigAutoConfiguration {

    private WebConfigProperties webConfigProperties;

    public WebConfigAutoConfiguration(WebConfigProperties webConfigProperties) {
        this.webConfigProperties = webConfigProperties;
    }

    @Bean
    public FilterRegistrationBean<CostTimeFilter> costFilter(WebConfigProperties webConfigProperties) {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        filterRegistrationBean.setFilter(new CostTimeFilter(webConfigProperties.getCostfilter()));
        ArrayList<String> objects = new ArrayList<>();
        objects.add("/*");
        filterRegistrationBean.setUrlPatterns(objects);
        return filterRegistrationBean;
    }
}
