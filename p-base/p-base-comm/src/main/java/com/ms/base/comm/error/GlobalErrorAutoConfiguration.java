package com.ms.base.comm.error;

import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.web.ResourceProperties;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <b>description</b>： <br>
 * <b>time</b>：2018-07-31 14:49 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Configuration
@ConditionalOnProperty(prefix = "ms.globalerror", name = "enabled", matchIfMissing = true)
@AutoConfigureBefore(ErrorMvcAutoConfiguration.class)
@EnableConfigurationProperties({ServerProperties.class, ResourceProperties.class, GlobalErrorConfigProperties.class})
public class GlobalErrorAutoConfiguration {

    @Bean
    public GlobalErrorController defaultErrorController(ErrorAttributes errorAttributes, ServerProperties serverProperties, GlobalErrorConfigProperties globalErrorConfigProperties) {
        return new GlobalErrorController(errorAttributes, serverProperties.getError(), globalErrorConfigProperties);
    }


    @Bean
    public GlobalErrorAttributes errorAttributes() {
        return new GlobalErrorAttributes();
    }

}
