package com.ms.base.comm.cloud;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * <b>description</b>：结果异常 <br>
 * <b>time</b>：2018-08-02 10:30 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ResultException extends RuntimeException {
    /**
     * 结果
     */
    private ResultDto resultDto;
}
