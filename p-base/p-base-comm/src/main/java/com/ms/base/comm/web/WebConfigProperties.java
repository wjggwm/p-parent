package com.ms.base.comm.web;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * <b>description</b>：webconfig配置信息 <br>
 * <b>time</b>：2018-07-27 10:52 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@ConfigurationProperties(WebConfigProperties.PREFIX)
@Data
public class WebConfigProperties {
    public static final String PREFIX = "com.ms.base.comm.web.webconfig";
    private CostTimeFilterProperties costfilter = new CostTimeFilterProperties();
}
