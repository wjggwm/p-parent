package com.ms.base.comm.error;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * <b>description</b>： <br>
 * <b>time</b>：2018-07-31 17:02 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Data
@ConfigurationProperties(prefix = GlobalErrorConfigProperties.PREFIX)
public class GlobalErrorConfigProperties {
    public static final String PREFIX = "com.ms.base.comm.error.globalerror";
    private boolean enabled = true;
    private WebType webType = WebType.API;
}
