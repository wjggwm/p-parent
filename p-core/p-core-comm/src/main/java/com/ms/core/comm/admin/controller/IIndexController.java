package com.ms.core.comm.admin.controller;

import com.ms.base.comm.cloud.ResultDto;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * <b>description</b>： <br>
 * <b>time</b>：2018-07-26 12:34 <br>
 * <b>author</b>： ready likun_557@163.com
 */
public interface IIndexController {

    @RequestMapping("/")
    ResultDto<String> index();

    @RequestMapping("/sleep/{timeout}")
    ResultDto<String> sleep(@PathVariable("timeout") long timeout, @RequestParam(value = "error", required = false) Integer i) throws Exception;

    @RequestMapping("/test1/{i}")
    ResultDto<Integer> test1(@PathVariable("i") int i) throws Exception;

}
