package com.ms.core.comm.admin.controller;

import com.ms.base.comm.cloud.ResultDto;
import com.ms.base.comm.page.PageConst;
import com.ms.base.comm.page.PageModel;
import com.ms.core.comm.admin.model.DemoModel;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <b>description</b>： <br>
 * <b>time</b>：2018-07-30 19:05 <br>
 * <b>author</b>： ready likun_557@163.com
 */
public interface IDemoController {

    /**
     * 插入
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/demo/insert")
    ResultDto<DemoModel> insert(@RequestBody DemoModel model) throws Exception;

    /**
     * 详情
     *
     * @param id
     * @return
     * @throws Exception
     */
    @RequestMapping("/demo/detail/{id}")
    ResultDto<DemoModel> detail(@PathVariable("id") long id) throws Exception;

    /**
     * 删除
     *
     * @param id
     * @return
     * @throws Exception
     */
    @RequestMapping("/demo/delete/{id}")
    ResultDto<Integer> delete(@PathVariable("id") long id) throws Exception;

    /**
     * 获取分页数据
     *
     * @param page 第几页
     * @param rows 每页多少条
     * @return
     * @throws Exception
     */
    @RequestMapping("/demo/list/{page}/{rows}")
    ResultDto<PageModel<DemoModel>> list(@PathVariable(PageConst.PAGE_KEY) int page, @PathVariable(PageConst.ROWS_KEY) int rows) throws Exception;
}
