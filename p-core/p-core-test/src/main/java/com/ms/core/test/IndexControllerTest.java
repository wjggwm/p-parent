package com.ms.core.test;

import com.ms.base.comm.cloud.ResultDto;
import com.ms.base.comm.cloud.ResultUtil;
import com.ms.base.comm.util.FrameUtil;
import com.ms.core.api.admin.IndexClient;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

/**
 * <b>description</b>： <br>
 * <b>time</b>：2018-07-26 12:34 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@RestController
public class IndexControllerTest implements ApplicationContextAware {
    @Autowired
    private IndexClient indexClient;

    private ApplicationContext applicationContext;

    @RequestMapping("/")
    public ResultDto<String> index() {
        return this.indexClient.index();
    }

    @RequestMapping("/sleep/{timeout}")
    public ResultDto<String> sleep(@PathVariable("timeout") long timeout, @RequestParam(value = "error", required = false) Integer i) throws Exception {
        return ResultUtil.ok(this.indexClient.sleep(timeout, i));
    }

    @RequestMapping("/test1/{i}")
    public ResultDto<Integer> test1(@PathVariable("i") int i) throws Exception {
        return ResultUtil.ok(this.indexClient.test1(i));
    }

    @RequestMapping("/bean/{name}")
    public ResultDto<Object> bean(@PathVariable("name") String name) {
        Object bean = this.applicationContext.getBean(name);
        return ResultUtil.ok(ResultUtil.successData(Objects.nonNull(bean) ? bean.toString() : null));
    }

    @RequestMapping("/test2/{msg}")
    public ResultDto<String> test2(@PathVariable("msg") String msg) {
        FrameUtil.throwBaseException(msg);
        return ResultUtil.success();
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
