package com.ms.core.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

/**
 * <b>description</b>： <br>
 * <b>time</b>：2018-07-27 14:46 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@ConditionalOnProperty(prefix = ApiAutoConfiguration.PREFIX, name = "enabled", matchIfMissing = true)
@Configuration
@EnableFeignClients
public class ApiAutoConfiguration {
    private static final Logger LOGGER = LoggerFactory.getLogger(ApiAutoConfiguration.class);
    public static final String PREFIX = "ms.core.api";
}
