package com.ms.core.service.admin.dao;

import com.ms.base.jdbc.dao.IBaseDao;
import com.ms.core.comm.admin.model.DemoModel;

/**
 * <b>description</b>：测试数据访问接口 <br>
 * <b>time</b>：2018-08-07 19:09:08 <br>
 * <b>author</b>：ready likun_557@163.com
 */
public interface IDemoDao extends IBaseDao<DemoModel> {
}