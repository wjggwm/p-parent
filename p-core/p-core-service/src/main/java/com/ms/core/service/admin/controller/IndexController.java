package com.ms.core.service.admin.controller;

import com.ms.base.comm.cloud.ResultDto;
import com.ms.base.comm.cloud.ResultUtil;
import com.ms.core.comm.admin.controller.IIndexController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * <b>description</b>： <br>
 * <b>time</b>：2018-07-26 12:34 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@RestController
@Slf4j
public class IndexController implements IIndexController {

    public ResultDto<String> index() {
        log.info("哈哈");
        return ResultUtil.successData("hello");
    }

    public ResultDto<String> sleep(@PathVariable("timeout") long timeout, @RequestParam(value = "error", required = false) Integer i) throws Exception {
        TimeUnit.SECONDS.sleep(timeout);
        if (i != null && i == 1) {
            throw new Exception("error 了!");
        }
        return ResultUtil.successData("success");
    }

    public ResultDto<Integer> test1(@PathVariable("i") int i) throws Exception {
        int result = 10 / i;
        return ResultUtil.successData(result);
    }
}
