package com.ms.core.service.admin.mapper;

import com.ms.base.jdbc.dao.IBaseMapper;
import com.ms.core.comm.admin.model.DemoModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * <b>description</b>：测试mapper <br>
 * <b>time</b>：2018-08-07 19:09:08 <br>
 * <b>author</b>：ready likun_557@163.com
 */
@Mapper
public interface DemoMapper extends IBaseMapper<DemoModel> {
}