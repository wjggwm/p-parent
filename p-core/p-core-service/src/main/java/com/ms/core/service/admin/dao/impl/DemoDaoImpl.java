package com.ms.core.service.admin.dao.impl;

import com.ms.base.jdbc.dao.BaseDaoImpl;
import com.ms.base.jdbc.dao.IBaseMapper;
import com.ms.core.comm.admin.model.DemoModel;
import com.ms.core.service.admin.dao.IDemoDao;
import com.ms.core.service.admin.mapper.DemoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * <b>description</b>：测试数据访问实现类 <br>
 * <b>time</b>：2018-08-07 19:09:08 <br>
 * <b>author</b>：ready likun_557@163.com
 */
@Component
public class DemoDaoImpl extends BaseDaoImpl<DemoModel> implements IDemoDao {
    @Autowired
    private DemoMapper demoMapper;

    @Override
    protected String getPrimaryKeyName() {
        return "id";
    }

    @Override
    public IBaseMapper<DemoModel> getBaseMapper() {
        return this.demoMapper;
    }
}