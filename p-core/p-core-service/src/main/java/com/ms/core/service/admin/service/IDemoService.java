package com.ms.core.service.admin.service;

import com.ms.base.jdbc.service.IBaseService;
import com.ms.core.comm.admin.model.DemoModel;

/**
* <b>description</b>：测试业务接口 <br>
* <b>time</b>：2018-08-07 19:09:08 <br>
* <b>author</b>：ready likun_557@163.com
*/
public interface IDemoService extends IBaseService<DemoModel> {
}