package com.ms.core.service.admin.controller;

import com.ms.base.comm.cloud.ResultDto;
import com.ms.base.comm.cloud.ResultUtil;
import com.ms.base.jdbc.datasource.DsType;
import com.ms.base.comm.page.PageModel;
import com.ms.base.jdbc.page.PageUtil;
import com.ms.core.comm.admin.controller.IDemoController;
import com.ms.core.comm.admin.model.DemoModel;
import com.ms.core.service.admin.service.IDemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * <b>description</b>： <br>
 * <b>time</b>：2018-07-26 15:26 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@RestController
public class DemoController implements IDemoController {
    @Autowired
    private IDemoService demoService;

    /**
     * 插入
     *
     * @param model
     * @return
     * @throws Exception
     */
    public ResultDto<DemoModel> insert(@RequestBody DemoModel model) throws Exception {
        model = this.demoService.insert(model);
        return ResultUtil.successData(model);
    }

    /**
     * 详情
     *
     * @param id
     * @return
     * @throws Exception
     */
    public ResultDto<DemoModel> detail(@PathVariable("id") long id) throws Exception {
        return ResultUtil.successData(this.demoService.getModelById(id, DsType.MASTER));
    }

    /**
     * 删除
     *
     * @param id
     * @return
     * @throws Exception
     */
    public ResultDto<Integer> delete(@PathVariable("id") long id) throws Exception {
        return ResultUtil.successData(this.demoService.deleteById(id));
    }

    /**
     * 获取所有数据
     *
     * @return
     * @throws Exception
     */
    public ResultDto<PageModel<DemoModel>> list(@PathVariable(PageUtil.PAGE_KEY) int page, @PathVariable(PageUtil.ROWS_KEY) int rows) throws Exception {
        PageModel<DemoModel> pageModel = this.demoService.getPageModel(null, page, rows, DsType.SLAVE);
        return ResultUtil.successData(pageModel);
    }
}
