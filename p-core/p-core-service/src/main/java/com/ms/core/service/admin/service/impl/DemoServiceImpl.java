package com.ms.core.service.admin.service.impl;

import com.ms.base.jdbc.dao.IBaseDao;
import com.ms.base.jdbc.service.BaseServiceImpl;
import com.ms.core.comm.admin.model.DemoModel;
import com.ms.core.service.admin.dao.IDemoDao;
import com.ms.core.service.admin.service.IDemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <b>description</b>：测试业务实现类 <br>
 * <b>time</b>：2018-08-07 19:09:08 <br>
 * <b>author</b>：ready likun_557@163.com
 */
@Service
public class DemoServiceImpl extends BaseServiceImpl<DemoModel> implements IDemoService {
    @Autowired
    private IDemoDao demoDao;

    @Override
    public IBaseDao<DemoModel> getBaseDao() {
        return this.demoDao;
    }
}